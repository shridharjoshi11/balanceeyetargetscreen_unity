﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using NetMQ;
using NetMQ.Sockets;
using UnityEngine;

namespace Cyclops.BalanceEye.Common
{
    public enum IpcTokens
    {
        StartStop = 1,
        TargetType = 2,
        TargetMovemnt = 3,
        FOV = 4,
        DOTPOS = 5,
        None = 0
    }

    public class CommunicatorIPC
    {
        private SubscriberSocket subSocket;
        private PublisherSocket pubSocket;
        string subadressl;

        public CommunicatorIPC(string pubAddress, string subAddress, bool isClient)
        {
            subadressl = subAddress;
            AsyncIO.ForceDotNet.Force();
            NetMQConfig.Cleanup();
            pubSocket = new PublisherSocket();
            pubSocket.Options.SendHighWatermark = 1000;

            pubSocket.Connect(pubAddress);
           
            Debug.Log("Success");
        }

        public void Publish(string msg, string param)
        {
            pubSocket.SendFrame(msg + "#" + param);
        }

        bool isQuit = false;
        NetMQPoller poller;

        Thread clientThread;
        private System.Object lockkk = new System.Object();

        bool isStop = false;
        Token mesagTken;
        /// <summary>
        /// Subscribe to type of IPC commands
        /// </summary>
        /// <param name="msgToken">
        /// token can be configured for : IPC_App_VRT, IPC_App_Target, IPC_App_CloudService.
        /// </param>
        public void Subscribe(Token msgToken)
        {
            mesagTken = msgToken;
            clientThread = new Thread(OnSubsribe);
            clientThread.Start();

            //clientThread = new Thread(OnSubsribe);
            //clientThread.Start();

            //// these event will be raised by the Poller
            //subSocket.ReceiveReady += (s, a) =>
            //{
            //    // receive won't block as a message is ready
            //    string packet = a.Socket.ReceiveFrameString();
            //    // send a response
            //     Mediator.Notify(msgToken, packet);

            //};
            //// start polling (on this thread)
            //poller.RunAsync();

            //Task.Factory.StartNew(() =>
            // {
            //     Debug.Log("CommunicatorIPC1");

            //     while (!isQuit)
            //      {
            //          string packet = subSocket.ReceiveFrameString(Encoding.UTF8);
            //          Debug.Log(packet);
            //         if(packet.Contains("QUIT"))
            //         {
            //             isQuit = true;
            //             pubSocket.Close();
            //             subSocket.Close();
            //             pubSocket.Dispose();
            //             subSocket.Dispose();

            //             continue;
            //         }
            //         // Mediator.Notify(msgToken, packet);
            //         string str = packet.ToString();
            //         // Debug.Log("main" + str);
            //         string[] strlist = str.Split('#');
            //         //  Debug.Log("main" + str[0]);
            //         if (strlist[0] == "TargetType")
            //         {
            //             if (strlist[1] == "optakinetic")
            //             {
            //                 GlobalValue.choice = 0;
            //             }

            //             if (strlist[1] == "rotate")
            //             {
            //                 GlobalValue.choice = 1;
            //             }

            //             if (strlist[1] == "subjectivemidpoint")
            //             {
            //                 GlobalValue.choice = 2;
            //             }

            //             if (strlist[1] == "smoothpersuit")
            //             {
            //                 GlobalValue.choice = 3;
            //             }

            //         }

            //         if (strlist[0] == "StartStop")
            //         {
            //             GlobalValue.IsGameRunning = (strlist[1] == "0") ? false : true;
            //         }
            //     }
            // });
        }

        private void OnSubsribe(object obj)
        {
            using ( subSocket = new SubscriberSocket())
            {
                subSocket.Options.ReceiveHighWatermark = 1000;
                subSocket.Connect(subadressl);

                subSocket.SubscribeToAnyTopic();

                while (!isStop)
                {
                    string packet = subSocket.ReceiveFrameString(Encoding.UTF8);
                    Mediator.Notify(mesagTken, packet);
                }
                subSocket.Disconnect(subadressl);
                pubSocket.Disconnect(subadressl);

            }
            NetMQConfig.Cleanup();

        }

        public void Dispose()
        {
            lock (lockkk) isStop = true;
           
            clientThread.Abort();
            Debug.Log("QIUTTT");
            //poller.StopAsync();
            //poller.Dispose();
            pubSocket.Dispose();
            subSocket.Dispose();
            pubSocket.Close();
            subSocket.Close();
            NetMQConfig.Cleanup();
        }

    }
}