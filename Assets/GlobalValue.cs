﻿
using System;
using Cyclops;
using Cyclops.BalanceEye.Common;
using System.Windows;
using System.Drawing;

public  class GlobalValue
{
    //{
    //    [MenuItem("Tools/MyTool/Do It in C#")]
    //    static void DoIt()
    //    {
    //        EditorUtility.DisplayDialog("MyTool", "Do It in C# !", "OK", "");
    //    }

    public GlobalValue()
    {

    }

    public static bool LineAppear=false;
    public static int choice = 0;
    public static bool IsGameRunning = false;
    public static bool IsHorizontal = true; 
    public static float Mode = 0;
    public static float freq = 0;
    public static float Pre_mode=0;
    public static float Cur_mode=0;
    public static int flagA =0;
    public static int flagB =0;
    public static bool deleteobj=false;
    public static bool reset=false;
    public static bool reset2 = false;
    public static float anglediff =0;
    public static bool Isapplication_Quit = false;
    public static PositionPoint FOV = new PositionPoint();
    public static PositionPoint DotPosition = new PositionPoint();

    public static void  initilize()
    {
        communicator = new CommunicatorIPC(recPort, sendcPort, true);
        isIntialized = true;
    }
      
    public static int CurrentScreenIdx;
    public static int LastScreenIdx;

    private static string recPort = "tcp://127.0.0.1:5011";
    private static string sendcPort = "tcp://127.0.0.1:5010";

    public static CommunicatorIPC communicator;

    public static bool isIntialized = false;

    public static bool IsVGA { get; internal set; }
    public static string KeyStroke { get; internal set; }
}
public struct PositionPoint
{
    public int x, y;
    public PositionPoint(int px, int py)
    {
        x = px;
        y = py;
    }
}