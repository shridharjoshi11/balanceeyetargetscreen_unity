﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalMov : MonoBehaviour
{
    float speed;
    public float startx;

    GameObject cam;

    bool flag1 = true;
    bool flag2 = true;
    float Heightperdegree;
    float Widthperdegree;
    private float pixelsPerDegree;
    private Vector3 screenMidpoint;
    public int mode;
    // Vector3 pos;
    void Start()
    {
      

        if (Display.displays.Length == 2)
            Display.displays[0].Activate();

        pixelsPerDegree = Screen.height / (GlobalValue.FOV.y * 2);
        screenMidpoint = Camera.main.WorldToScreenPoint(new Vector3(0f, 0f, 0f));
        Debug.Log("Pixelsperdegree " + pixelsPerDegree);
        Vector3 posVectPixels = new Vector3(getCurrentXValue(GlobalValue.FOV.x), getCurrentYValue(GlobalValue.FOV.y), 0);
        Vector3 posVect = Camera.main.ScreenToWorldPoint(posVectPixels);

        Widthperdegree = -posVect.x / 30.0f;
        Heightperdegree = -posVect.y / 20.0f;

        // pos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.height, Screen.width, 0));
        // Debug.Log("Position of h and w "+pos);

        //pos= Camera.main.ScreenToViewportPoint(new Vector3(Screen.height, Screen.width, 0));
        // Debug.Log("Position of h and w " + pos);
        cam = GameObject.Find("Main Camera");
        Debug.Log(Time.fixedDeltaTime);

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (GlobalValue.Mode)
        {
            case 0:
                setCameraPosition();
                if (GlobalValue.IsGameRunning)
                {
                    speed = 10 * Widthperdegree;
                    leftToright();
                }
                break;
            case 1:
                setCameraPosition();
                if (GlobalValue.IsGameRunning)
                {
                    speed = 10 * Widthperdegree;
                    rightToleft();
                }
                break;
            case 4:
                setCameraPosition();
                if (GlobalValue.IsGameRunning)
                {
                    speed = 20 * Widthperdegree;
                    leftToright();
                }
                break;
            case 5:
                setCameraPosition();
                if (GlobalValue.IsGameRunning)
                {
                    speed = 20 * Widthperdegree;
                    rightToleft();
                }
                break;
            case 8:
                setCameraPosition();
                if (GlobalValue.IsGameRunning)
                {
                    speed = 40 * Widthperdegree;
                    leftToright();
                }
                break;
            case 9:
                setCameraPosition();
                if (GlobalValue.IsGameRunning)
                {
                    speed = 40 * Widthperdegree;
                    rightToleft();
                }
                break;
            case 12:
                setCameraPosition();
                if (GlobalValue.IsGameRunning)
                {
                    speed = 60 * Widthperdegree;
                    leftToright();
                }
                break;
            case 13:
                setCameraPosition();
                if (GlobalValue.IsGameRunning)
                {
                    speed = 60 * Widthperdegree;
                    rightToleft();
                }
                break;
        }
    }


    private void leftToright()
    {
        if (transform.position.x < 16.6f)
        {
            transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), transform.position.y, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(-18.9f + (speed * Time.deltaTime), transform.position.y, transform.position.z);
        }
    }
    private void rightToleft()
    {
        if (transform.position.x > -18.9f)
        {
            transform.position = new Vector3(transform.position.x - (speed * Time.deltaTime), transform.position.y, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(16.6f - (speed * Time.deltaTime), transform.position.y, transform.position.z);
        }
    }

    private void setCameraPosition()
    {
        if (cam.transform.position.x != 0)
        {
            cam.transform.position = new Vector3(0, transform.position.y, -5);
            flag1 = true;
            flag2 = true;
        }
    }

    public void HFourtyDegree()
    {
        GlobalValue.IsGameRunning = true;
        GlobalValue.Mode = 6;
    }
    public void HSixtyDegree()
    {
        GlobalValue.IsGameRunning = true;
        GlobalValue.Mode = 7;
    }


    private float getCurrentXValue(int x)
    {

        if (x < 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else
            return screenMidpoint.x;
    }

    private float getCurrentYValue(int x)
    {
        if (x < 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else
            return screenMidpoint.y;
    }
}