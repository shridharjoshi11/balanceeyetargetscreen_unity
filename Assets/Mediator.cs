﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.Windows;

namespace Cyclops
{
    /// <summary>
    /// Implementation of Mediator Pattern for interaction between modules
    /// </summary>
    static public class Mediator
    {
       static IDictionary<Token, List<Action<object>>> pl_dict = new Dictionary<Token, List<Action<object>>>();
       public static object TestSelected;

        static public void Register(Token token, Action<object> callback)
        {
            if (!pl_dict.ContainsKey(token))
            {
                var list = new List<Action<object>>();
                list.Add(callback);
                pl_dict.Add(token, list);
            }
            else
            {
                bool found = false;
                foreach (var item in pl_dict[token])
                    if (item.Method.ToString() == callback.Method.ToString())
                        found = true;
                if (!found)
                    pl_dict[token].Add(callback);
            }
        }

        public static void Notify(object testSelected, object newValue)
        {
            Debug.Log("Notify1");
            throw new NotImplementedException();
           
        }

        static public void Unregister(Token token, Action<object> callback)
        {
            if (pl_dict.ContainsKey(token))
                pl_dict[token].Remove(callback);
        }

        /// <summary>
        /// to be tesed..added by Ajith
        /// </summary>
        /// <param name="token"></param>
        static public void Unregister(Token token)
        {
            if (pl_dict.ContainsKey(token))
                pl_dict.Remove(token);
        }

        static public void Notify(Token token, object args)
        {
            if (pl_dict.ContainsKey(token))
                foreach (var callback in pl_dict[token].ToList())
                    callback(args);

            Debug.Log("Notify2");
        }



    }

    public  enum Token
    {
        ShowPage,
        PupilPoint,
        ProtocolTreeSelected,
        IPC_App_VRT,
        IPC_App_Target,
        IPC_App_CloudService,
        StartStop
    }
}



















































