﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DotTargetScript : MonoBehaviour
{
    private float pixelsPerDegree;
    private Vector3 screenMidpoint;
    private Vector3 sizeOfDotInPixel;
  //  private float Units_per_pixel;

    // Start is called before the first frame update
    void Start()
    {

        if (Display.displays.Length == 2)
            Display.displays[0].Activate();
        //var p1 = Camera.main.ScreenToWorldPoint(Vector3.zero);
        //var p2 = Camera.main.ScreenToWorldPoint(Vector3.right);



        pixelsPerDegree = (Screen.height) / (GlobalValue.FOV.y * 2.0f);
        screenMidpoint = Camera.main.WorldToScreenPoint(new Vector3(0f, 0f, 0f));
        sizeOfDotInPixel = Camera.main.WorldToScreenPoint(transform.localScale);
        //Units_per_pixel = Vector3.Distance(p1, p2);

        Vector3 posVectPixelss = new Vector3(getCurrentXValue(1.5f), getCurrentYValue(1.5f), 0); // 1 degree size of cube
        var posVects = Camera.main.ScreenToWorldPoint(posVectPixelss);
        transform.localScale = new Vector3(posVects.x, posVects.y, 0);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GlobalValue.IsGameRunning)
        {
            Vector3 posVectPixels = new Vector3(getCurrentXValue(GlobalValue.DotPosition.x), getCurrentYValue(GlobalValue.DotPosition.y), 0);
            Transform camTrans = Camera.main.transform;
            float dist = Vector3.Dot(transform.position - camTrans.position, camTrans.forward);
            posVectPixels.z = dist;

            var posVect = Camera.main.ScreenToWorldPoint(posVectPixels);
            if (GlobalValue.DotPosition.y < 0)
                transform.position = new Vector3(-posVect.x, -(posVect.y + (transform.localScale.y / 2)), 0);
            else if (GlobalValue.DotPosition.y > 0)
                transform.position = new Vector3(-posVect.x, -(posVect.y - (transform.localScale.y / 2)), 0);
            else
                transform.position = new Vector3(-posVect.x, -posVect.y, 0);
        }
    }

    private float getCurrentXValue(float x)
    {

        if (x < 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else
            return screenMidpoint.x;
    }

    private float getCurrentYValue(float x)
    {
        if (x < 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else
            return screenMidpoint.y;
    }
}
