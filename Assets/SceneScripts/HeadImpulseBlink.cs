﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadImpulseBlink : MonoBehaviour
{
    public float Units_per_pixel;
    MeshRenderer cubeRenderer;
    bool flag1 = true;
    bool flag2 = false;
    public GameObject rectangular_bar;
     private int pixelsPerDegree;
    private Vector3 screenMidpoint;
    private float rectangular_bar_degree_width = 30;

    void Start()
    {
        var p1 = Camera.main.ScreenToWorldPoint(Vector3.zero);
        var p2 = Camera.main.ScreenToWorldPoint(Vector3.right);
        Units_per_pixel = Vector3.Distance(p1, p2);

        cubeRenderer = GetComponent<MeshRenderer>();
        pixelsPerDegree = Screen.height / (GlobalValue.FOV.y * 2);
      
        var width_rectangular_bar = Units_per_pixel * rectangular_bar_degree_width * pixelsPerDegree;

        rectangular_bar.transform.localScale = new Vector3(width_rectangular_bar, rectangular_bar.transform.localScale.y, rectangular_bar.transform.localScale.z);
    }

    // Update is called once per frame
    void FixedUpdate()
    {     
        if(GlobalValue.IsGameRunning)
        {
            if (flag1 == true && flag2 == false)
            {
                cubeRenderer.material.color = Color.green;
                StartCoroutine(Delay_generate());
                flag2 = true;
            }
            if (flag1 == false && flag2 == true)
            {
                cubeRenderer.material.color = Color.red;
                StartCoroutine(Delay_generate());
                flag2 = false;
            }
        }       
    }

    IEnumerator Delay_generate()
    {
        yield return new WaitForSeconds(1);
        if (flag1 == true)
            flag1 = false;
        else
            flag1 = true;
    }
 
}
