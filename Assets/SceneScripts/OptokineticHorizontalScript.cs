﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptokineticHorizontalScript : MonoBehaviour
{
    float speed;
    public float startx;
    public GameObject ob;
    public GameObject cam;
    bool flag1 = true;
    bool flag2 = true;
    float Heightperdegree;
    float Widthperdegree;
    private int pixelsPerDegree;
    private Vector3 screenMidpoint;
   // Vector3 pos;
    void Start()
    {
        if (Display.displays.Length == 2)
            Display.displays[0].Activate();

        pixelsPerDegree = Screen.height / (GlobalValue.FOV.y * 2);
        screenMidpoint = Camera.main.WorldToScreenPoint(new Vector3(0f, 0f, 0f));
        Debug.Log("Pixelsperdegree " + pixelsPerDegree);
        Vector3 posVectPixels = new Vector3(getCurrentXValue(GlobalValue.FOV.x), getCurrentYValue(GlobalValue.FOV.y), 0);
        Vector3 posVect = Camera.main.ScreenToWorldPoint(posVectPixels);

        Widthperdegree = -posVect.x/ 30.0f;
         Heightperdegree = -posVect.y / 20.0f;

        // pos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.height, Screen.width, 0));
       // Debug.Log("Position of h and w "+pos);
        //pos= Camera.main.ScreenToViewportPoint(new Vector3(Screen.height, Screen.width, 0));
        // Debug.Log("Position of h and w " + pos);
    }

    // Update is called once per frame
    void Update()
    {
        if(GlobalValue.IsGameRunning)
        {
            Debug.Log("Height " + Screen.height);
            Debug.Log("width " + Screen.width);       
            if (GlobalValue.Mode == 0)
            {
                speed = 10 * Widthperdegree;
                if (flag1 == false || flag2 == false)
                {
                    cam.transform.Rotate(0, 0, -90);
                    flag1 = true;
                    flag2 = true;
                }

                if (transform.position.x < 32.7)
                {
                    transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(startx + (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }

                if (ob.transform.position.x < 32.7)
                {
                    ob.transform.position = new Vector3(ob.transform.position.x + (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }
                else
                {
                    ob.transform.position = new Vector3(startx + (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }
            }

            if (GlobalValue.Mode == 1)
            {
                if (flag1 == false || flag2 == false)
                {
                    cam.transform.Rotate(0, 0, -90);
                    flag1 = true;
                    flag2 = true;
                }
                speed = 10 * Widthperdegree;
                if (!flag1)
                {
                    cam.transform.Rotate(0, 0, -90);
                    flag1 = false;
                }

                if (transform.position.x > -41)
                {
                    transform.position = new Vector3(transform.position.x - (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(32.60f - (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }

                if (ob.transform.position.x > -41)
                {
                    ob.transform.position = new Vector3(ob.transform.position.x - (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }
                else
                {
                    ob.transform.position = new Vector3(32.60f - (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }

            }
            if (GlobalValue.Mode == 2)
            {
                if (flag2 && flag1)
                {
                    cam.transform.Rotate(0, 0, 90);
                    flag2 = false;
                }
                speed = 10 * Heightperdegree;

                if (transform.position.x < 32.7)
                {
                    transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(startx + (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }

                if (ob.transform.position.x < 32.7)
                {
                    ob.transform.position = new Vector3(ob.transform.position.x + (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }
                else
                {
                    ob.transform.position = new Vector3(startx + (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }
            }

            if (GlobalValue.Mode == 3)
            {
                if (flag1 && flag2)
                {
                    cam.transform.Rotate(0, 0, 90);
                    flag1 = false;
                }
                speed = 10 * Heightperdegree;

                if (transform.position.x > -41)
                {
                    transform.position = new Vector3(transform.position.x - (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(32.60f - (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }

                if (ob.transform.position.x > -41)
                {
                    ob.transform.position = new Vector3(ob.transform.position.x - (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }
                else
                    ob.transform.position = new Vector3(32.60f - (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
            }

            if (GlobalValue.Mode == 4)
            {
                if (flag1 == false || flag2 == false)
                {
                    cam.transform.Rotate(0, 0, -90);
                    flag1 = true;
                    flag2 = true;
                }

                speed = 20 * Widthperdegree;
                if (!flag1)
                {
                    cam.transform.Rotate(0, 0, -90);
                    flag1 = false;
                }
                if (transform.position.x < 32.7)
                {
                    transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(startx + (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }

                if (ob.transform.position.x < 32.7)
                {
                    ob.transform.position = new Vector3(ob.transform.position.x + (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }
                else
                {
                    ob.transform.position = new Vector3(startx + (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }
            }

            if (GlobalValue.Mode == 5)
            {
                if (flag1 == false || flag2 == false)
                {
                    cam.transform.Rotate(0, 0, -90);
                    flag1 = true;
                    flag2 = true;
                }

                speed = 20 * Widthperdegree;
                if (transform.position.x > -41)
                {
                    transform.position = new Vector3(transform.position.x - (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(32.60f - (speed * Time.deltaTime), transform.position.y, transform.position.z);
                }

                if (ob.transform.position.x > -41)
                {
                    ob.transform.position = new Vector3(ob.transform.position.x - (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }
                else
                {
                    ob.transform.position = new Vector3(32.60f - (speed * Time.deltaTime), ob.transform.position.y, ob.transform.position.z);
                }

            }
        }
    }


    private float getCurrentXValue(int x)
    {

        if (x < 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else
            return screenMidpoint.x;
    }

    private float getCurrentYValue(int x)
    {
        if (x < 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else
            return screenMidpoint.y;
    }
}

