﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMPScriptLineMovementScript : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 2.0f;
    public GameObject ob1;
    GameObject[] ob2 = new GameObject[30];
    bool flag = false;
    private int pixelsPerDegree;
    private Vector3 screenMidpoint;
    private Vector3 initialPosLine = new Vector3(-5, 0, 0);
    void Start()
    {
        if (Display.displays.Length == 2)
            Display.displays[0].Activate();
        pixelsPerDegree = Screen.height / (GlobalValue.FOV.y * 2);
        screenMidpoint = Camera.main.WorldToScreenPoint(new Vector3(0f, 0f, 0f));
        GlobalValue.KeyStroke = "";
    }

    // Update is called once per frame
    void Update()
    {



        if (GlobalValue.KeyStroke == "Right" )
        {
            transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), 0, 0);
            GlobalValue.KeyStroke = string.Empty;
        }
        else if (GlobalValue.KeyStroke == "Left" )
        {
            transform.position = new Vector3(transform.position.x - (speed * Time.deltaTime), 0, 0);
            GlobalValue.KeyStroke = string.Empty;
        }
        if (GlobalValue.LineAppear)
        {
            for (int l = 0; l < 15; l++)
            {
                ob2[l] = Instantiate(ob1, new Vector3(0, l / 2.0f, -1), Quaternion.identity);
                ob2[15 + l] = Instantiate(ob1, new Vector3(0, -l / 2.0f, -1), Quaternion.identity);
            }
            GlobalValue.LineAppear = false;
            var linePosVect = Camera.main.WorldToScreenPoint(transform.position);
            var angleDiff = transform.position.x;

            if (linePosVect.x > screenMidpoint.x)
                angleDiff = linePosVect.x - screenMidpoint.x;
            else
                angleDiff = -(screenMidpoint.x - linePosVect.x);
            var deviationInAngle = angleDiff / pixelsPerDegree;
            Debug.Log("Distance from center\t" + deviationInAngle);

            GlobalValue.communicator.Publish("SMPDistance", deviationInAngle.ToString("0.00"));           // ob2 = Instantiate(ob1, new Vector3(0, 0, 0), Quaternion.identity);
            // ob2.transform.localScale = new Vector3(0.02f, 12f, 0f);

        }

        if (GlobalValue.reset)
        {
            transform.position = initialPosLine;
            for (int l = 0; l < 15; l++)
            {
                Destroy(ob2[l]);
                Destroy(ob2[15 + l]);
            }
            GlobalValue.reset = false;
        }
    }

}