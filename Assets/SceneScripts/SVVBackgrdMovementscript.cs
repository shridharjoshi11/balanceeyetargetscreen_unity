﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SVVBackgrdMovementscript : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 target = new Vector3(0.0f, 0.0f, 0.0f);
    private Vector3 target2 = new Vector3(0.0f, 0.0f, 1.0f);
    bool flag = false;
    bool is_rotated=false;
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if(GlobalValue.Mode==0)
        {
          if(GlobalValue.IsGameRunning)
            {
                if (flag)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y , transform.position.z+25);
                    flag = false;
                }
                transform.RotateAround(target, -target2, 60/4 * Time.deltaTime);
                is_rotated = true;
            }
        }
        else if (GlobalValue.Mode == 1)
        {
            if (GlobalValue.IsGameRunning)
            {
                if (flag)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 25);
                    flag = false;
                }
                transform.RotateAround(target, target2, 60 / 4 * Time.deltaTime);
                is_rotated = true;
            }
        }
           
        else if (GlobalValue.Mode == 2)
           // if (GlobalValue.IsGameRunning)
            {
                if(flag==false)
                {
                    flag = true;
                    transform.position = new Vector3(transform.position.x , transform.position.y , transform.position.z -25);
                }       
            }
        else if (!GlobalValue.IsGameRunning && is_rotated)
        {
            if (flag == false)
            {
                flag = true;
                transform.position = new Vector3(transform.position.x , transform.position.y , transform.position.z -25);
                is_rotated = false;
            }
        }
        //if (GlobalValue.deleteobj==true)
        //{
        //    Destroy(gameObject);
        //    GlobalValue.deleteobj = false;
        //}

    }   
}
