﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using System.Threading;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class SVVBackground : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject ob;
    int i = 0;
    public GameObject ob1;
    int m;
    int p;
    int flag = 1;
    private Vector3 target = new Vector3(0.0f, 0.0f, 0.0f);
    private Vector3 target2 = new Vector3(0.0f, 0.0f, 1.0f);
    List<Vector3> klist = new List<Vector3>();
    private Vector3 position;
    private Vector3 diff;
    double distanc;
    double difforiginmod;
    private Vector3 distanceorigin;
    GameObject ob2;
    public GameObject ob4;
    GameObject[] ob3 = new GameObject[30];
    public Stopwatch timer;
    void Start()
    {
        //Thread y = new Thread(background);
        //y.Start();
        position = new Vector3(0, 0, 0);
        klist.Add(position);
        m = 1;
        timer = new Stopwatch();
        timer.Start();
       //Debug.Log("Time of loop start  "+ System.DateTime.Now.Millisecond);

        while (i < 1500)
        {
            position = new Vector3((Random.Range(-15.0f, 15.0f)), Random.Range(-15.0f, 15.0f), 15.0f);
            distanceorigin = klist[0] - position;
            difforiginmod = Math.Sqrt(Math.Pow(distanceorigin.x, 2) + Math.Pow(distanceorigin.y, 2));
            if (difforiginmod > 3)
            {
                for (p = 0; p < m; p++)
                {
                    diff = klist[p] - position;

                    distanc = Math.Sqrt(Math.Pow(diff.x, 2) + Math.Pow(diff.y, 2));
                    if (distanc < 0.800f)
                    {
                        flag = -1;
                    }
                }
                if (flag == 1)
                {
                    klist.Add(position);
                    m++;
                }
                flag = 1;
            }
            i++;
        }
        Debug.Log("Time of loop end " + timer.Elapsed);
       // Debug.Log("Time of loop end "+ System.DateTime.Now.Millisecond);

        for (p = 1; p < m; p++)
        {
            ob2 = Instantiate(ob, klist[p], Quaternion.identity);
            float j = Random.Range(-0.5f, 0.5f);                    //  scaling the size
            ob2.transform.localScale = new Vector3(j, j, j);
        }
        Debug.Log("Time of loop end 2 " + timer.Elapsed);
        if (Display.displays.Length == 2)
            Display.displays[0].Activate();

    }

    // Update is called once per frame
    void Update()
    {

        if (GlobalValue.LineAppear)
        {
            for (int l = 0; l < 15; l++)
            {
                ob3[l] = Instantiate(ob1, new Vector3(0, l / 2.0f, 0), Quaternion.identity);
                ob3[15 + l] = Instantiate(ob1, new Vector3(0, -l / 2.0f, 0), Quaternion.identity);
            }
            GlobalValue.LineAppear = false;

            float diff = 0;
            Debug.Log("Rotation angle " + ob4.transform.rotation.eulerAngles.z);
            float angle = ob4.transform.rotation.eulerAngles.z;
            if (angle < 90.0f)
                diff = (90.0f - angle);
            else if (angle > 90.0f && angle < 180.0f)
                diff = -(angle - 90.0f);
            else if (angle > 180.0f && angle < 270.0f)
                diff = (270.0f - angle);
            else
                diff = -(angle - 270.0f);
            Debug.Log("Angle difference\t" + diff);
            GlobalValue.communicator.Publish("SVVAngle", diff.ToString("0.00"));
        }
    }
     static void background()
    {
        for (int i = 0; i < 20; i++)
            Debug.Log("i " + i);
    }
}
