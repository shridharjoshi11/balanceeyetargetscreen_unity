﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SVVLineRotate : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 target = new Vector3(0.0f, 0.0f, 0.0f);
    private Vector3 target2 = new Vector3(0.0f, 0.0f, 1.0f);
    private Vector3 target3 = new Vector3(0.0f, 0.0f, -1.0f);
    GameObject[] ob2 = new GameObject[24];

    void Start()
    {
       
    }
    const int Limit = 1;
    private int count = 0;
    // Update is called once per frame
    void FixedUpdate()
    {

        Debug.Log("KEY------" + GlobalValue.KeyStroke);
        if (count == Limit)
        {
            GlobalValue.KeyStroke = "";
            count = 0;
        }

      // if (GlobalValue.KeyStroke == "Left")
       if (GlobalValue.KeyStroke == "Left")
       //if(Input.GetKey(KeyCode.LeftArrow))
        {
            transform.RotateAround(target, target2, 0.5f);
            count++;
            //GlobalValue.KeyStroke = "";

        }
       
       else if (GlobalValue.KeyStroke == "Right")
        //if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.RotateAround(target, target3, 0.5f);
            count++;
           //  GlobalValue.KeyStroke = "";
        }
        Debug.Log("Time.delta "+Time.deltaTime);
        // transform.localEulerAngles = new Vector3(0, 0, 0);
        ////if (GlobalValue.reset2)
        ////{
        ////    for (int i = 0; i < 10; i++)
        ////        transform.RotateAround(target, target3, 10);
        ////    GlobalValue.reset2 = false;
        ////    // transform.position = new Vector3(1, 1, 0);
        ////    //transform.localEulerAngles = new Vector3(0, 0, 0);
        ////}

        //To delete the unwanted gameobject after switching to new scene
        if (GlobalValue.deleteobj)
        {
            Destroy(gameObject);
            GlobalValue.deleteobj = false;
        }
    }
}

