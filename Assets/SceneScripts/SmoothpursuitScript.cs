﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using System.Threading;
using System;

public class SmoothpursuitScript : MonoBehaviour
{
    float radians;
    public Stopwatch timer;
    private float pixelsPerDegree;
    private Vector3 screenMidpoint;
    bool is_timer_start= true;
    public float Units_per_pixel;
    float box_size_degree = 2;

    // Start is called before the first frame update
    void Start()
    {
        if (Display.displays.Length == 2)
            Display.displays[0].Activate();
        var p1 = Camera.main.ScreenToWorldPoint(Vector3.zero);
        var p2 = Camera.main.ScreenToWorldPoint(Vector3.right);
        Units_per_pixel = Vector3.Distance(p1, p2);

        pixelsPerDegree = (Screen.height) / (GlobalValue.FOV.y * 2.0f);
        screenMidpoint = Camera.main.WorldToScreenPoint(new Vector3(0f, 0f, 0f));
        timer = new Stopwatch();

        Vector3 posVectPixelss = new Vector3(getCurrentXValue(1.5f), getCurrentYValue(1.5f), 0);
        var posVects = Camera.main.ScreenToWorldPoint(posVectPixelss);
        transform.localScale = new Vector3(posVects.x, posVects.y, 0);

        var box_size = Units_per_pixel * box_size_degree * pixelsPerDegree;
       //transform.localScale=new Vector3(box_size,box_size,box_size);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GlobalValue.IsGameRunning)
        {           
            radians = 2 * Mathf.PI * GlobalValue.Mode;
            Vector3 posVect = new Vector3(GlobalValue.FOV.x * pixelsPerDegree * Units_per_pixel, GlobalValue.FOV.y * pixelsPerDegree * Units_per_pixel, 0);
            if (is_timer_start)
            {
                timer.Start();
                is_timer_start = false;
            }
            TimeSpan ts = timer.Elapsed;
            double t =ts.TotalSeconds;
            float time = (float)t;
           
            if (GlobalValue.IsHorizontal)
                    transform.position = new Vector3(posVect.x * (Mathf.Sin(radians * time)), 0, 0);
                else
                    transform.position = new Vector3(0, (posVect.y+(transform.localScale.y/2.0f)) * (Mathf.Sin(radians * time)), 0);        
        }
        else
        {
            is_timer_start = true;
            timer.Reset();
        }     
        
    }

    private float getCurrentXValue(float x)
    {

        if (x < 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else
            return screenMidpoint.x;
    }

    private float getCurrentYValue(float x)
    {
        if (x < 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else
            return screenMidpoint.y;
    }
}
