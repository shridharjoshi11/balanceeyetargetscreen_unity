﻿using Cyclops;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Windows;
using System.Drawing;
public class StartMain : MonoBehaviour
{
    // Start is called before the first frame update
    private int currentscene;
    //public GameObject ob;
    //public string names;
    int i;

    void Start()
    {
        //var v = Camera.allCameras;


        //Display.displays[1].Activate();
        //Cams.targetDisplay = 1;
        //PlayerPrefs.SetInt("UnitySelectMonitor", 1);

        if (Display.displays.Length == 2)
        {
            Display.displays[0].Activate();
            var Cams = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            Cams.targetDisplay = 0;
        }

        Mediator.Unregister(Token.IPC_App_Target, OnCommunicationCommand);
        if (!GlobalValue.isIntialized)
        {
            GlobalValue.initilize();
            GlobalValue.communicator.Subscribe(Cyclops.Token.IPC_App_Target);
        }
        Screen.fullScreen = true;
        Mediator.Register(Token.IPC_App_Target, OnCommunicationCommand);
        currentscene = SceneManager.GetActiveScene().buildIndex;
        //   GlobalValue.KeyStroke = ""; sc

    }

    // Update is called once per frame
    void Update()
    {
        if (GlobalValue.choice != currentscene)
        {
          //  GlobalValue.deleteobj = true;
            SceneManager.LoadScene(GlobalValue.choice);
        }
           

        if (GlobalValue.Pre_mode != GlobalValue.Cur_mode)
        {
            //GlobalValue.deleteobj = true;
            SceneManager.LoadScene(currentscene);
            GlobalValue.Pre_mode = GlobalValue.Cur_mode;
        }

        if (GlobalValue.flagA > GlobalValue.flagB)
        {
            if (currentscene == 2 || currentscene==3 )
            {
                GlobalValue.reset = true;
                GlobalValue.reset2 = true;
                GlobalValue.flagA = 0;
                GlobalValue.flagB = 0;
            }
            else
            {
               // GlobalValue.deleteobj = true;
             //   SceneManager.LoadScene(GlobalValue.choice);
                SceneManager.LoadScene(currentscene);

                GlobalValue.flagA = 0;
                GlobalValue.flagB = 0;
            }
            
        }
        if (GlobalValue.Isapplication_Quit)
        {
            GlobalValue.communicator.Dispose();
            GlobalValue.communicator = null;
            Application.Quit();
        }

    }
    void OnApplicationQuit()
    {
        //CancelInvoke();
        //StopAllCoroutines();
        GlobalValue.communicator.Dispose();
        GlobalValue.communicator = null;
        Application.Quit();

        // UnityEditor.EditorApplication.isPlaying = false;
        //  Mediator.Unregister(Token.IPC_App_Target, OnCommunicationCommand);
        //EditorApplication.Exit(0);
    }
   
    private void OnDisable()
    {
        //CancelInvoke();
        //StopAllCoroutines();

    }

    public static void OnCommunicationCommand(object obj)
    {
        string str = obj.ToString();
        string[] strlist = str.Split('#');
        if (strlist[0] == "Load")
        {
            if (strlist[1] == "Optokinetic Test")
            {
                // GlobalValue.IsGameRunning = false;
                GlobalValue.choice = 1;
            }
            if (strlist[1] == "Subjective Visual Vertical")
            {
                // GlobalValue.IsGameRunning = false;
                GlobalValue.choice = 2;
            }

            else if (strlist[1] == "Subjective Mid Point")
            {
                // GlobalValue.IsGameRunning = false;
                GlobalValue.choice = 3;
            }

            else if (strlist[1] == "Smooth Pursuit")
            {
                //  GlobalValue.IsGameRunning = false;
                GlobalValue.choice = 4;
            }
            else if (strlist[1] == "standBy")
            {
                GlobalValue.choice = 0;
            }
            else if (strlist[1] == "Saccades" || strlist[1] == "Calibration" || strlist[1] == "Gaze Test")
            {
                GlobalValue.choice = 5;
            }
            else if (strlist[1] == "Head Impulse")
            {
                GlobalValue.choice = 6;

            }

            //if (strlist[1] == "freq")
            //{

            //}

        }
        else if (strlist[0] == "StartStop")
        {
            GlobalValue.IsGameRunning = (strlist[1] == "False") ? false : true;
          
            if (GlobalValue.choice == 2 || GlobalValue.choice == 3)
            {
                if (GlobalValue.IsGameRunning == false)
                    GlobalValue.LineAppear = true;

            }
            if (GlobalValue.IsGameRunning)
            {
                GlobalValue.flagA++;
            }
        }
        else if (strlist[0] == "Mode")
        {
            GlobalValue.Pre_mode = GlobalValue.Mode;
            GlobalValue.Cur_mode = float.Parse(strlist[1]);
            GlobalValue.Mode = (float.Parse(strlist[1]));
            GlobalValue.flagA = 0;
            GlobalValue.flagB = 0;

        }
        else if (strlist[0] == "OrientationChange")
        {
            GlobalValue.IsHorizontal = (strlist[1] == "False") ? false : true;

        }
        else if (strlist[0] == "SpotPosition")
        {
            //   GlobalValue.DotPosition = (PositionPoint)strlist[1] ;
            var values = strlist[1].Split(new char[] { ',', '}', '{' });
            GlobalValue.DotPosition.x = int.Parse(values[0]);
            GlobalValue.DotPosition.y = int.Parse(values[1]);

        }
        else if (strlist[0] == "FOVChange")
        {
            var values = strlist[1].Split(new char[] { ',', '}', '{' });
            GlobalValue.FOV.x = int.Parse(values[0]);
            GlobalValue.FOV.y = int.Parse(values[1]);

        }
        else if (strlist[0] == "IsVGA")
        {
            GlobalValue.IsVGA = (strlist[1] == "False") ? false : true;

        }
        else if (strlist[0] == "KeyStroke")
        {
            GlobalValue.KeyStroke = (strlist[1]);
        }
        else if (strlist[0] == "Terminate")
        {
            GlobalValue.Isapplication_Quit = true;
        }
    }
}
