﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubjectiveMedPointScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject ob1;
    bool is_moved=false;

    Vector3 position;
    float[] speed = new float[500];
    float j;
    int i = 0;
    int k = 0;
    GameObject[] obj = new GameObject[500];
    bool flag = false;


    void Start()
    {
        if (Display.displays.Length == 2)
            Display.displays[0].Activate();
      //  Debug.Log("Time of loop start  " + Time.time +": " + System.DateTime.Now.Millisecond);
        for (i = 0; i < 500; i++)
        {
            obj[i] = Instantiate(ob1, new Vector3(Random.Range(-14, 14f), Random.Range(-10, 10f), 1), Quaternion.identity);
            j = Random.Range(-0.4f, 0.4f);                  //  scaling the size
            obj[i].transform.localScale = new Vector3(j, j, j);
            speed[i] = Random.Range(1.0f, 5.0f);
        }
      //  Debug.Log("Time of loop end " + Time.time + ": " + System.DateTime.Now.Millisecond);
    }

    // Update is called once per frame
    void Update()
    {
        if (GlobalValue.Mode == 0)
        {
            if (GlobalValue.IsGameRunning)
            {
                for (i = 0; i < 500; i++)
                {
                    if (flag)
                    {
                        for (k = 0; k < 500; k++)
                            obj[k].transform.position = new Vector3(obj[k].transform.position.x + 30, obj[k].transform.position.y, obj[k].transform.position.z);
                        flag = false;
                    }
                    if (obj[i].transform.position.x > 14)
                    {
                        obj[i].transform.position = new Vector3(-14, obj[i].transform.position.y, obj[i].transform.position.z);
                    }
                    obj[i].transform.position = new Vector3((obj[i].transform.position.x + (speed[i] * Time.deltaTime)), obj[i].transform.position.y, obj[i].transform.position.z);
                }
                is_moved = true;
            }
               
            
        }
        else if (GlobalValue.Mode == 1)
        {
            if (GlobalValue.IsGameRunning)
            {
                for (i = 0; i < 500; i++)
                {
                    if (flag)
                    {
                        for (k = 0; k < 500; k++)
                            obj[k].transform.position = new Vector3(obj[k].transform.position.x + 30, obj[k].transform.position.y, obj[k].transform.position.z);
                        flag = false;
                    }
                    if (obj[i].transform.position.x < -14)
                    {
                        obj[i].transform.position = new Vector3(14, obj[i].transform.position.y, obj[i].transform.position.z);
                    }
                    obj[i].transform.position = new Vector3((obj[i].transform.position.x - (speed[i] * Time.deltaTime)), obj[i].transform.position.y, obj[i].transform.position.z);

                }
                is_moved = true;
            }
               
        }
        else if (GlobalValue.Mode == 2)
        {
            for (i = 0; i < 500; i++)
            {
                if (flag == false)
                {
                    if (obj[i].transform.position.x > -14)
                        obj[i].transform.position = new Vector3(obj[i].transform.position.x - 30, obj[i].transform.position.y, obj[i].transform.position.z);
                }
            }

            flag = true;
        }

        if (!GlobalValue.IsGameRunning && is_moved)
        {
            for (i = 0; i < 500; i++)
            {
                if (flag == false)
                {
                    if (obj[i].transform.position.x > -14)
                        obj[i].transform.position = new Vector3(obj[i].transform.position.x - 30, obj[i].transform.position.y, obj[i].transform.position.z);
                }
            }
            is_moved = false;
            flag = true;
        }
    }
}
