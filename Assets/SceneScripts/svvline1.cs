﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class svvline1 : MonoBehaviour
{
    //Start is called before the first frame update
    public GameObject ob1;
    public static int lineDotcount = 40;
    GameObject [] ob2= new GameObject[lineDotcount];
    void Start()
    {
        //for (int l = 0; l < 20; l++)
        //{
        //    ob2[l] = Instantiate(ob1, new Vector3(l / 8.0f, 0, 0), Quaternion.identity);
        //    ob2[15 + l] = Instantiate(ob1, new Vector3(-l / 8.0f, 0, 0), Quaternion.identity);
        //}
        createline();
    }

    // Update is called once per frame
    void Update()
    {
        if (GlobalValue.reset2)
        {
            for (int l = 0; l < lineDotcount/2; l++)
            {
                Destroy(ob2[l]);
                Destroy(ob2[(lineDotcount / 2) + l]);
               // ob2[l].transform.position = new Vector3((l/ 8.0f), 0, 0);
               // ob2[15 + l].transform.position = new Vector3((-l / 8.0f), 0, 0);
            }
            createline();
            GlobalValue.reset2 = false;
        }
    }

    public void createline()
    {
        for (int l = 0; l < lineDotcount/2; l++)
        {
            ob2[l] = Instantiate(ob1, new Vector3(l / 8.0f, 0, 0), Quaternion.identity);
            ob2[(lineDotcount/2 )+ l] = Instantiate(ob1, new Vector3(-l / 8.0f, 0, 0), Quaternion.identity);
        }
    }


}
