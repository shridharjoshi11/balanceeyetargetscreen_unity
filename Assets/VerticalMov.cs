﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalMov : MonoBehaviour
{
    float speed;
    public float startx;

    GameObject cam;

    bool flag1 = true;
    bool flag2 = true;
    float Heightperdegree;
    float Widthperdegree;
    private float pixelsPerDegree;
    private Vector3 screenMidpoint;
    public int mode;
    // Vector3 pos;
    void Start()
    {
        

        if (Display.displays.Length == 2)
            Display.displays[0].Activate();

        pixelsPerDegree = Screen.height / (GlobalValue.FOV.y * 2.0f);
        screenMidpoint = Camera.main.WorldToScreenPoint(new Vector3(0f, 0f, 0f));
        Debug.Log("Pixelsperdegree " + pixelsPerDegree);
        Vector3 posVectPixels = new Vector3(getCurrentXValue(GlobalValue.FOV.x), getCurrentYValue(GlobalValue.FOV.y), 0);
        Vector3 posVect = Camera.main.ScreenToWorldPoint(posVectPixels);

        Widthperdegree = -posVect.x / 30.0f;
        Heightperdegree = -posVect.y / 20.0f;

        // pos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.height, Screen.width, 0));
        // Debug.Log("Position of h and w "+pos);

        //pos= Camera.main.ScreenToViewportPoint(new Vector3(Screen.height, Screen.width, 0));
        // Debug.Log("Position of h and w " + pos);
        cam = GameObject.Find("Main Camera");
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        {
            switch (GlobalValue.Mode)
            {
                case 2:
                    setCameraPosition();
                    if (GlobalValue.IsGameRunning)
                    {
                        speed = 10 * Widthperdegree;
                        topTodown();
                    }
                    break;
                case 3:
                    setCameraPosition();
                    if (GlobalValue.IsGameRunning)
                    {
                        speed = 10 * Widthperdegree;
                        downTotop();
                    }
                    break;
                case 6:
                    setCameraPosition();
                    if (GlobalValue.IsGameRunning)
                    {
                        speed = 20 * Widthperdegree;
                        topTodown();
                    }
                    break;
                case 7:
                    setCameraPosition();
                    if (GlobalValue.IsGameRunning)
                    {
                        speed = 20 * Widthperdegree;
                        downTotop();
                    }
                    break;
                case 10:
                    setCameraPosition();
                    if (GlobalValue.IsGameRunning)
                    {
                        speed = 40 * Widthperdegree;
                        topTodown();
                    }
                    break;
                case 11:
                    setCameraPosition();
                    if (GlobalValue.IsGameRunning)
                    {
                        speed = 40 * Widthperdegree;
                        downTotop();
                    }
                    break;
                case 14:
                    setCameraPosition();
                    if (GlobalValue.IsGameRunning)
                    {
                        speed = 60 * Widthperdegree;
                        topTodown();
                    }
                    break;
                case 15:
                    setCameraPosition();
                    if (GlobalValue.IsGameRunning)
                    {
                        speed = 60 * Widthperdegree;
                        downTotop();
                    }
                    break;
            }
        }

    }

    private void setCameraPosition()
    {
        if (cam.transform.position.x != 50)
        {
            cam.transform.position = new Vector3(50, 0, -5);
            flag1 = false;
        }
    }

    private void topTodown()
    {
        if (transform.position.y > -8.86f)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - (speed * Time.deltaTime), transform.position.z);
        }
        else
        {
            transform.position = new Vector3(transform.position.x, 8.86f - (speed * Time.deltaTime), transform.position.z);
        }
    }

    private void downTotop()
    {
        if (transform.position.y < 8.86f)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + (speed * Time.deltaTime), transform.position.z);
        }
        else
        {
            transform.position = new Vector3(transform.position.x, -8.86f + (speed * Time.deltaTime), transform.position.z);
        }
    }

    public void VFourtyDegree()
    {
        GlobalValue.IsGameRunning = true;
        GlobalValue.Mode = 8;
    }
    public void VSixtyDegree()
    {
        GlobalValue.IsGameRunning = true;
        GlobalValue.Mode = 9;
    }


    private float getCurrentXValue(int x)
    {

        if (x < 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else
            return screenMidpoint.x;
    }

    private float getCurrentYValue(int x)
    {
        if (x < 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else
            return screenMidpoint.y;
    }
}