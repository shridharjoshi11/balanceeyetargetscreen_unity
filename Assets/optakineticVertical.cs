﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class optakineticVertical : MonoBehaviour
{
    float speed;
    public float startx;

    GameObject cam;

    bool flag1 = true;
    bool flag2 = true;
    float Heightperdegree;
    float Widthperdegree;
    private int pixelsPerDegree;
    private Vector3 screenMidpoint;
    public int mode;
    // Vector3 pos;
    void Start()
    {
        if (Display.displays.Length == 2)
            Display.displays[0].Activate();

        pixelsPerDegree = Screen.height / (GlobalValue.FOV.y * 2);
        screenMidpoint = Camera.main.WorldToScreenPoint(new Vector3(0f, 0f, 0f));
        Debug.Log("Pixelsperdegree " + pixelsPerDegree);
        Vector3 posVectPixels = new Vector3(getCurrentXValue(GlobalValue.FOV.x), getCurrentYValue(GlobalValue.FOV.y), 0);
        Vector3 posVect = Camera.main.ScreenToWorldPoint(posVectPixels);

        Widthperdegree = -posVect.x / 30.0f;
        Heightperdegree = -posVect.y / 20.0f;

        // pos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.height, Screen.width, 0));
        // Debug.Log("Position of h and w "+pos);

        //pos= Camera.main.ScreenToViewportPoint(new Vector3(Screen.height, Screen.width, 0));
        // Debug.Log("Position of h and w " + pos);
        cam = GameObject.Find("Main Camera");
   
    }

    // Update is called once per frame
    void FixedUpdate()
    {


        {
            Debug.Log("Height " + Screen.height);
            Debug.Log("width " + Screen.width);
            //if (GlobalValue.Mode == 0)
            //{
            //    speed = 10 * Widthperdegree;
            //    if (flag1 == false || flag2 == false && cam.transform.position.x != 0)
            //    {
            //        cam.transform.position = new Vector3(0, transform.position.y, transform.position.z);
            //        flag1 = true;
            //        flag2 = true;
            //    }            
            //}

            //if (GlobalValue.Mode == 1)
            //{
            //    if (flag1 == false || flag2 == false && cam.transform.position.x != 0)
            //    {
            //        cam.transform.position = new Vector3(0, transform.position.y, transform.position.z);
            //        flag1 = true;
            //        flag2 = true;
            //    }
            //}
            if (GlobalValue.Mode == 2)
            {
                if ( cam.transform.position.x != 50)
                {
                    cam.transform.position = new Vector3(50, 0, transform.position.z);
                    flag2 = false;
                }
                speed = 10 * Heightperdegree;

                if (GlobalValue.IsGameRunning)
                    if (transform.position.y < 6)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y + (speed * Time.deltaTime), transform.position.z);
                    }
                    else
                    {
                        transform.position = new Vector3(transform.position.x, startx + (speed * Time.deltaTime), transform.position.z);
                    }
            }

            if (GlobalValue.Mode == 3)
            {
                if ( cam.transform.position.x != 50)
                {
                    cam.transform.position = new Vector3(50,0, transform.position.z);
                    flag1 = false;
                }
                speed = 10 * Heightperdegree;

                if (GlobalValue.IsGameRunning)
                    if (transform.position.y > -6)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y - (speed * Time.deltaTime), transform.position.z);
                    }
                    else
                    {
                        transform.position = new Vector3(transform.position.x, 6 - (speed * Time.deltaTime), transform.position.z);
                    }

            }

            //if (GlobalValue.Mode == 4)
            //{
            //    if (flag1 == false || flag2 == false && cam.transform.position.x != 0)
            //    {
            //        cam.transform.position = new Vector3(0, transform.position.y, transform.position.z);
            //        flag1 = true;
            //        flag2 = true;
            //    }

            //    speed = 20 * Widthperdegree;
            //    //if (!flag1)
            //    //{
            //    //    cam.transform.Rotate(0, 0, -90);
            //    //    flag1 = false;
            //    //}

                
            //}

            //if (GlobalValue.Mode == 5)
            //{
            //    if (flag1 == false || flag2 == false && cam.transform.position.x != 0)
            //    {
            //        cam.transform.position = new Vector3(0, transform.position.y, transform.position.z);
            //        flag1 = true;
            //        flag2 = true;
            //    }

            //    speed = 20 * Widthperdegree;

                

            //}
        }
    }


    private float getCurrentXValue(int x)
    {

        if (x < 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.x - (x * pixelsPerDegree);
        else
            return screenMidpoint.x;
    }

    private float getCurrentYValue(int x)
    {
        if (x < 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else if (x > 0)
            return screenMidpoint.y - (x * pixelsPerDegree);
        else
            return screenMidpoint.y;
    }
}